
package main

import (
	"syscall/js"
)

func Handle(_ js.Value, args []js.Value) interface{} {

	name := args[0].Get("name")

	return map[string]interface{}{
		"message": "🏓 pong " + name.String() + " 😃😃😃",
	}
}

func main() {
	println("🤖: ping wasm loaded")

	js.Global().Set("Handle", js.FuncOf(Handle))

	<-make(chan bool)
}
