🖐 `wasm.js.loader.js` must be updated when the GalaGo platform is upgraded

### Get the `wasm.js.loader.js` file

```bash
wget https://gitlab.com/galago.fun/bush.baby/-/raw/main/helpers/wasm.js.loader.js
```