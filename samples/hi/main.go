package main

import (
	"syscall/js"
)

/*
go run main.go -cmd=build -name=hi -source=samples/hi/main.go -ver=2.0.0
go run main.go -cmd=activate -name=hi -ver=2.0.0
go run main.go -cmd=call -name=hi -json='{"name":"Bob"}' -headers='{"demo_token":"plouf"}'
*/

func Handle(_ js.Value, args []js.Value) interface{} {
	// get an object
	jsonPayload := args[0]
	// get members of an object
	name := jsonPayload.Get("name").String()

	requestHeaders := args[1]
	println("🦊")
	//println(requestHeaders)
	// headers fields neam always in minus case
	demoToken := requestHeaders.Get("demo_token").String()

	return map[string]interface{}{
		"message": "👋 Hello " + name,
		"token":   demoToken,
		"author":  "@k33g_org 🐼",
	}
}

func main() {
	println("🤖: hi wasm loaded")

	js.Global().Set("Handle", js.FuncOf(Handle))

	<-make(chan bool)
}
