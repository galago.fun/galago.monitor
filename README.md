# GalaGo.CLI

> - version: 0.0.6 📦
> - version: 0.0.5
> - version: 0.0.4 ❌
> - version: 0.0.3 ❌
> - version: 0.0.2 ❌
> - version: 0.0.1 ❌
> - version: 0.0.0 ❌

## Build

```bash
# examples
GOOS=darwin GOARCH=amd64 go build -o galago.cli
GOOS=linux GOARCH=amd64 go build -o galago.cli
```

## Run

> example:
```bash
export GALAGO_URL="https://8080-tan-antlion-0n8k2rc8.ws-eu16.gitpod.io"
export GALAGO_TOKEN="ILOVEPANDA"
./galago.cli -cmd=data
```

## Tips: Find IP from the host name

```bash
IP=$(ping -c 1 galago.local | awk -F"[()]" 'NR==1{print $2}')
echo $IP
```
