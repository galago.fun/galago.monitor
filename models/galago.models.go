package models

import (
	"encoding/json"
)

type GalagoConfig struct {
	Url           string
	Token         string
	DataApi       string
	CallApi       string
	DeployApi     string
	ActivateApi   string
	DeactivateApi string
	ScaleApi      string
	RemoveApi     string
	KillApi       string
	BuildApi      string
}

type GalaGoFunctionData struct {
	Function         string
	Version          string
	IsDefaultVersion bool
	Description      string
	ProcessIndex     int64
	Pid              int64
	Started          string
	Ended            string
	Duration         int64 // 🧐
	Status           string
	Counter          int64
}

/*
type GalagoValue struct {
	Message string
}
*/

// This structure is an "image" of the result json payload returned by the GalaGo server
type GalagoResult struct {
	Failure        string
	Success        interface{}
	HttpStatusCode int
}

// TODO: handle error interface conversion
// panic: interface conversion: interface {} is string, not map[string]interface {}
func (r GalagoResult) Either(failure func(errorMessage string), success func(value map[string]interface{})) {
	if r.Failure != "" {
		failure(r.Failure)
	} else {
		success(r.Success.(map[string]interface{}))
	}
}

func (r GalagoResult) OfJsonString(httpStatusCode int, jsonString string) GalagoResult {
	json.Unmarshal([]byte(jsonString), &r)
	r.HttpStatusCode = httpStatusCode
	return r
}

type GalagoFunctionResult struct {
	Failure        string
	Success        interface{}
	HttpStatusCode int
}

func (r GalagoFunctionResult) Either(failure func(errorMessage string), success func(value interface{})) {
	if r.HttpStatusCode != 200 {
		failure(r.Failure)
	} else {
		success(r.Success)
	}
}

func (r *GalagoFunctionResult) SetSuccess(success string) {
	r.Success = success
}

func (r GalagoFunctionResult) OfJsonString(httpStatusCode int, jsonString string) GalagoFunctionResult {
	json.Unmarshal([]byte(jsonString), &r)
	r.HttpStatusCode = httpStatusCode
	if r.HttpStatusCode == 200 {
		r.SetSuccess(jsonString)
	}
	return r
}
