package api

import (
	"net/http"

	"gitlab.com/galago.fun/galago.cli/models"
)

/*
function_name="$2"
version_to_activate="$3"
url="${url_api}/functions/deactivate/${function_name}/${version_to_deactivate}"

curl -d "{}" \
	-H "Content-Type: application/json" \
	-H "ADMIN_GALAGO_TOKEN: ${galago_token}" \
	-X POST "${url}"
*/
func DeactivateFunction(config models.GalagoConfig, functionName string, functionVersion string) (int, string) {
	//bodyParams := bytes.NewReader([]byte(jsonString))

	req, _ := http.NewRequest(http.MethodPost, getDeactivateApiUrl(config, functionName, functionVersion), nil)
	req.Header.Add("admin_galago_token", config.Token)
	//req.Header.Set("Content-Type", "application/json")
	/*
		Body cannot be empty when content-type is set to 'application/json'
	*/

	return execPostRequest(req)

}
