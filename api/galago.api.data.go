package api

import (
	"io/ioutil"
	"net/http"
	"gitlab.com/galago.fun/galago.cli/models"
)

/*
curl -H "ADMIN_GALAGO_TOKEN: ${galago_token}" \
			"${url_api}/functions/processes/data"

# with the name of the function
function_name=$2
curl -H "ADMIN_GALAGO_TOKEN: ${galago_token}" \
			"${url_api}/functions/processes/data/${function_name}"
*/
func GetData(config models.GalagoConfig, functionName string) []byte {
	client := http.Client{}

	req, _ := http.NewRequest(http.MethodGet, getDataApiUrl(config, functionName), nil)
	req.Header.Add("admin_galago_token", config.Token)
	req.Header.Set("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	return body
}

