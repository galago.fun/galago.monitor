package api

import (
	"strconv"
	"gitlab.com/galago.fun/galago.cli/models"
)

func getDataApiUrl(config models.GalagoConfig, functionName string) string {
	if functionName != "" {
		return config.Url + config.DataApi + "/" + functionName
	} else {
		return config.Url + config.DataApi
	}
}

func getCallApiUrl(config models.GalagoConfig, functionName string, functionVersion string) string {
	if functionVersion != "" {
		return config.Url + config.CallApi + "/" + functionName + "/" + functionVersion
	} else {
		return config.Url + config.CallApi + "/" + functionName
	}
}

func getDeployApiUrl(config models.GalagoConfig, functionVersion string) string {
	//fmt.Println(config.Galago.Url + config.Galago.DeployApi + "/" + functionVersion)
	return config.Url + config.DeployApi + "/" + functionVersion
}

func getBuildApiUrl(config models.GalagoConfig, functionVersion string) string {
	//fmt.Println(config.Galago.Url + config.Galago.BuildApi + "/" + functionVersion)
	return config.Url + config.BuildApi + "/" + functionVersion
}

func getActivateApiUrl(config models.GalagoConfig, functionName string, functionVersion string) string {
	return config.Url + config.ActivateApi + "/" + functionName + "/" + functionVersion
}

func getDeactivateApiUrl(config models.GalagoConfig, functionName string, functionVersion string) string {
	return config.Url + config.DeactivateApi + "/" + functionName + "/" + functionVersion
}

func getScaleApiUrl(config models.GalagoConfig, functionName string, functionVersion string) string {
	return config.Url + config.ScaleApi + "/" + functionName + "/" + functionVersion
}

func getRemoveApiUrl(config models.GalagoConfig, functionName string, functionVersion string) string {
	return config.Url + config.RemoveApi + "/" + functionName + "/" + functionVersion
}

func getKillApiUrl(config models.GalagoConfig, functionName string, functionVersion string, processIndex int64) string {
	return config.Url + config.KillApi + "/" + functionName + "/" + functionVersion + "/" + strconv.FormatInt(processIndex, 10)
}
