package api

import (
	"bytes"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"

	"gitlab.com/galago.fun/galago.cli/models"
)

/*
function_name=$2
source_code=$3
function_version=$4
curl -F "${function_name}=@${source_code}" \
			-H "Content-Type: multipart/form-data" \
			-H "ADMIN_GALAGO_TOKEN: ${galago_token}" \
			-X POST ${url_api}/functions/build/${function_version}
*/
func BuildWasmFunction(config models.GalagoConfig, functionName string, functionVersion string, goSourcePath string) (int, string) {

	filePath := goSourcePath

	file, _ := os.Open(filePath)
	defer file.Close()

	bodyParams := &bytes.Buffer{}
	writer := multipart.NewWriter(bodyParams)

	part, _ := writer.CreateFormFile(functionName, filepath.Base(file.Name()))
	io.Copy(part, file)
	writer.Close()

	req, _ := http.NewRequest(http.MethodPost, getBuildApiUrl(config, functionVersion), bodyParams)
	req.Header.Add("admin_galago_token", config.Token)
	req.Header.Add("Content-Type", writer.FormDataContentType())
	//req.Header.Set("Content-Type", "application/json")

	return execPostRequest(req)
}
